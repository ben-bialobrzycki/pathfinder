﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAnimationMover : MonoBehaviour {

    public Vector3 StartPosition;
    public Vector3 EndPosition;

    bool headToEnd;
    

    public float Speed = 20.0f;
    // Use this for initialization
    void Start () {
        headToEnd = true;
        transform.position = StartPosition;
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 dest;
        if (headToEnd)
            dest = EndPosition;
        else
            dest = StartPosition;

        Vector3 nextPOs = Vector3.MoveTowards(transform.position, dest, Speed * Time.deltaTime);

        transform.position = nextPOs;

        if (Vector3.Distance(transform.position, dest) < 0.1f)
            headToEnd = !headToEnd;
        
	}
}
