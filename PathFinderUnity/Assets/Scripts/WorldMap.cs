﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    
    public class WorldMap : ILocalMap
    {
        public int Height { get; set; }
        public int Width { get; set; }

        public const float factor = 1.0f;

        Location[,] locations;
        public WorldMap()
        {
        }

        public Location GetAdjacentLocation(Location start,Direction direction)
        {
            switch (direction)
            {
                case Direction.Down: return GetLocation(start.x, start.y - 1);
                case Direction.Up: return GetLocation(start.x, start.y + 1);
                case Direction.Left: return GetLocation(start.x - 1, start.y);
                case Direction.Right: return GetLocation(start.x + 1,start.y);
            }
            return Location.NullLocation();

        }

       
        public delegate void ApplyToLocation(Location loc);
        public void ApplyToAllLocations(ApplyToLocation locationFunction)
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    locationFunction(locations[x, y]);
                }
            }
        }

        public void ApplyToLocationRange(ApplyToLocation locationFunction, int startx, int starty, int range)
        {
            for (int x = startx - range; x <= startx + range; x++)
            {
                for (int y = starty - range; y <= starty + range; y++)
                {
                    Location loc = GetLocation(x, y);
                    if (loc.IsNullLocation())
                        continue;
                    locationFunction(locations[x, y]);
                }
            }
        }


        public Location GetLocation(int x, int y)
        {
            if (x < 0 || x >= Width
                || y < 0 || y >= Height)
                return Location.NullLocation();
            return locations[x, y];
        }
        public void InitialiseMap(int width, int height, GameObject HiddenTilePreFab)
        {
            this.Width = width;
            this.Height = height;
            locations = new Location[width, height];
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    Location loc = new Location() { x = x, y = y };
                    Vector3 position = PositionForIndices(loc.x, loc.y);
                   // loc.SetTerrain(IsBlocked(position), IsFertile(position), IsWood(position), IsGold(position));
                    locations[x, y] = loc;

                }
            }

        }
 
        
        

        Vector3 PositionForMapIndices(int x, int y)
        {
            return new Vector3(x * factor, factor * y,0);
        }

        public Vector3 PositionForIndices(float x, float y)
        {
            return new Vector3(x * factor, factor * y,0);
        }

        
        List<Location> GetAllAdjacent(Location location)
        {
            List<Location> locs = new List<Location>();
            locs.Add(GetAdjacentLocation(location, Direction.Up));
            locs.Add(GetAdjacentLocation(location, Direction.Down));
            locs.Add(GetAdjacentLocation(location, Direction.Left));
            locs.Add(GetAdjacentLocation(location, Direction.Right));
            return locs;
        }
      


        public Location LocationForPosition(float x, float y)
        {
            int x_indice, y_indice;
            x_indice = (int)Mathf.Round(x / factor);
            y_indice = (int)Mathf.Round(y / factor);
            if (x < 0 || x > Width || y < 0 || y > Height)
                return Location.NullLocation();

            return GetLocation(x_indice, y_indice);
        }

        public bool IsBlocked(PathFinder.Node.NLocation end, IMovableUnit forUnit)
        {
            Location loc = GetLocation(end.x, end.y);
            if (loc.IsBlocked())
                return true;
         
            return false;
        }

        public int GetTerrainPenalty(IMovableUnit moveableUnit, PathFinder.Node.NLocation location)
        {
           
            return 0;
        }

        public Vector3 LocationToVector(PathFinder.Node.NLocation loc)
        {
            return this.PositionForIndices(loc.x, loc.y);
        }

        public PathFinder.Node.NLocation VectorToLocation(Vector3 vec)
        {
            var loc = this.LocationForPosition(vec.x, vec.y);
            return new PathFinder.Node.NLocation(loc.x, loc.y);
        }

        public bool ValidLocation(PathFinder.Node.NLocation node)
        {
            if (node.x < 0 || node.x >= Width || node.y < 0 || node.y >= Height)
                return false;
            return true;
        }
    }
}
