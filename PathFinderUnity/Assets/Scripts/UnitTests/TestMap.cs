﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;



public class TestMap : ILocalMap
{
    public struct MapCell
    {
        public int X;
        public int Y;
        //public GenTileBehaviour tileBehaviour;
        public int LayerMask;

        public bool HasLayer(int cellType)
        {
            return (LayerMask & cellType) == cellType;
        }
    }


    public int width = 40;//30;
    public int height = 40;//30;
    public MapCell[,] grid;
    public TestMap()
    {

    }


    public int Width
    {
        get
        {
            return width;
        }

        set
        {
            width = value;
        }
    }

    public void SetBlock(Cell startCell, int[,] testBlock)
    {
        testBlock = FlipBlock(testBlock);
        for (int y = 0; y < testBlock.GetLength(0); y++)
        {
            for (int x = 0; x < testBlock.GetLength(1); x++)
            {
                Cell gridCell = new Cell(startCell.X + x, startCell.Y + y);
                if (OffMap(gridCell))
                    continue;
                grid[gridCell.Y, gridCell.X].LayerMask = testBlock[y, x];
            }
        }
    }

    public int GetCellType(int x, int y)
    {
        return grid[y, x].LayerMask;
    }

    public bool HasLayer(int x, int y, CellType cellType)
    {
        return grid[y, x].HasLayer((int)cellType);
    }
    public static int[,] FlipBlock(int[,] block)
    {
        int[,] flipped = new int[block.GetLength(0), block.GetLength(1)];

        for (int y = 0; y < block.GetLength(0); y++)
        {
            for (int x = 0; x < block.GetLength(1); x++)
            {
                flipped[y, x] = block[(block.GetLength(0) - 1) - y, x];
            }
        }
        return flipped;
    }

    public void SetCell(Cell cell, CellType cellType)
    {
        grid[cell.Y, cell.X].LayerMask = (int)cellType;
    }
    public int Height { get { return height; } set { height = value; } }

    public void BuildDungeon()
    {
        ReCreateGrid();
    }



    void ReCreateGrid()
    {
        grid = new MapCell[height, width];
        ClearAllCells();
    }

    public bool ValidLocation(PathFinder.Node.NLocation node)
    {
        if (node.x < 0 || node.y < 0 || node.x >= Width || node.y >= Height)
            return false;
        return true;
    }

    public void SetSize(int width, int height)
    {
        this.width = width;
        this.height = height;
        ReCreateGrid();
    }

    public class MockUnit : IMovableUnit
    {
        public int FactionId
        {
            get
            {
                return 0;
            }
        }

        public int UnitTypeId { get { return 0; } }

        public bool NeedRoad { get { return false; } }

        public GameObject GameObject { get { return null; } }

        GameObject IMovableUnit.GameObject
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }


    int Clamp(int value, int min, int max)
    {
        if (value < min)
            return min;
        if (value > max)
            return max;
        return value;
    }


    void GetSurroundingCells(Cell centre, ref int[] surround)
    {
        Cell start = new Cell(centre.X - 1, centre.Y - 1);
        Cell end = new Cell(centre.X + 1, centre.Y + 1);

        int cellSurround = 0;
        for (int x = start.X; x <= end.X; x++)
        {
            for (int y = start.Y; y <= end.Y; y++)
            {
                if (centre.X == x && centre.Y == y)
                    continue;

                if (OffMap(new Cell(x, y)))
                    surround[cellSurround] = (int)CellType.None;
                else
                    surround[cellSurround] = grid[y, x].LayerMask;
                cellSurround++;
            }
        }

    }



    public void ClearAllCells()
    {
        RunOverAllCells((x, y) => { grid[y, x].LayerMask = (int)CellType.None; });
    }

    public void RunOverAllCells(ApplyToCell cellFunc)
    {
        RunOverCells(cellFunc, StartCell(), EndCell());
    }

    public enum CellType { None = 1, Ground = 4, Wall = 8, Debug = 16, Furniture = 32, Column = 64, Plant = 128 }

    public void RunOverCells(ApplyToCell cellFunc, Cell start, Cell end, bool leaveBorder = false)
    {
        for (int i = start.X; i <= end.X; i++)
        {

            if ((i >= width || i < 0) || (leaveBorder && i >= width - 2))
                continue;
            for (int j = start.Y; j <= end.Y; j++)
            {
                if (j >= height || j < 0 || (leaveBorder && j >= height - 2))
                    continue;
                cellFunc(i, j);

            }
        }

    }

    Cell StartCell()
    {
        return new Cell(0, 0);
    }

    Cell EndCell()
    {
        return new Cell(width, height);
    }



    bool OffMap(PathFinder.Node.NLocation end)
    {
        if (end.x < 0 || end.x >= width)
            return true;
        if (end.y < 0 || end.y >= height)
            return true;
        return false;
    }

    bool OffMap(Cell end)
    {
        if (end.X < 0 || end.X >= width)
            return true;
        if (end.Y < 0 || end.Y >= height)
            return true;
        return false;
    }
    public bool IsBlocked(PathFinder.Node.NLocation end, IMovableUnit moveableUnit)
    {
        if (end.x < 0 || end.x >= width)
            return true;
        if (end.y < 0 || end.y >= height)
            return true;

        return HasLayer(end.x, end.y, CellType.Wall) || HasLayer(end.x, end.y, CellType.Furniture) || HasLayer(end.x, end.y, CellType.Plant);

    }

    //bool HasLayer(int x, int y, CellType cellType )
    //{
    //    return (grid[y, x].LayerMask & (int)cellType) == (int)cellType; 
    //}


    public int GetTerrainPenalty(IMovableUnit moveableUnit, PathFinder.Node.NLocation location)
    {
        if (OffMap(location))
            return 10;
        if (grid[location.y, location.x].LayerMask == (int)CellType.Wall)
            return 5;
        return 0;
    }


    public Vector3 LocationToVector(Cell loc)
    {
        return LocationToVector(new PathFinder.Node.NLocation(loc.X, loc.Y));
    }

    public Vector3 LocationToVector(PathFinder.Node.NLocation loc)
    {
        return new Vector3(loc.x + 0.5f, loc.y + 0.5f, 0);
    }

    public PathFinder.Node.NLocation VectorToLocation(Vector3 vec)
    {
        return new PathFinder.Node.NLocation(System.Convert.ToInt32(vec.x - 0.5f), System.Convert.ToInt32(vec.y - 0.5f));
    }

    public struct Cell
    {
        public int X;
        public int Y;
        public Cell(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", X, Y);
        }
    }

    public delegate void ApplyToCell(int x, int y);

}

