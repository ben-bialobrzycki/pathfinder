﻿using Assets.Scripts;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;


public class PathFinderTests
{
    TestMap map;
    string path = @"C:\Temp\pathfindertest.txt";

    [Test]
    public void FindSimplePath()
    {
        map = new TestMap();
        map.SetSize(5, 5);

        // 0 0 0 0 0 
        // 0 A 1 0 0 
        // 0 0 1 B 0 
        // 0 0 1 0 0 
        // 0 0 0 0 0 


        map.SetCell(new TestMap.Cell(2, 1), TestMap.CellType.Wall);
        map.SetCell(new TestMap.Cell(2, 2), TestMap.CellType.Wall);
        map.SetCell(new TestMap.Cell(2, 3), TestMap.CellType.Wall);

        PathFinder pathFinder = new PathFinder(map);
        var A = new PathFinder.Node.NLocation(1, 3);
        var B = new PathFinder.Node.NLocation(3, 2);
        var path = pathFinder.FindShortPath(new TestMap.MockUnit(), A, B);
        List<PathFinder.Node.NLocation> nodePath = new List<PathFinder.Node.NLocation>();
        foreach (var x in path)
        {
            var node = map.VectorToLocation(x);
            nodePath.Add(node);
            map.SetCell(new TestMap.Cell(node.x, node.y), TestMap.CellType.Ground);
        }

        PrintMap(map);
        Assert.AreEqual(6, nodePath.Count);
        Assert.AreEqual(new PathFinder.Node.NLocation(1, 3), nodePath[0]);
        Assert.AreEqual(new PathFinder.Node.NLocation(1, 4), nodePath[1]);
        Assert.AreEqual(new PathFinder.Node.NLocation(2, 4), nodePath[2]);
        Assert.AreEqual(new PathFinder.Node.NLocation(3, 4), nodePath[3]);
        Assert.AreEqual(new PathFinder.Node.NLocation(3, 3), nodePath[4]);
        Assert.AreEqual(new PathFinder.Node.NLocation(3, 2), nodePath[5]);


    }

    char CharForCell(int cell)
    {
        switch (cell)
        {
            case (int)TestMap.CellType.None: return 'X';
            case (int)TestMap.CellType.Ground: return '0';
            case (int)TestMap.CellType.Wall: return 'W';
        }
        return '!';
    }
    void PrintMap(TestMap map)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine();
        for (int y = map.height - 1; y >= 0; y--)
        {
            for (int x = 0; x < map.width; x++)
            {
                sb.Append(CharForCell(map.grid[y, x].LayerMask));
            }
            sb.AppendLine();
        }

        Debug.Log(sb.ToString());
        PrintToFile(sb.ToString());
    }


    void PrintToFile(String map, bool append = true)
    {

        using (StreamWriter sw = new StreamWriter(path, append))
        {
            sw.Write(map);
        }
    }
}

