﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


namespace Assets.Scripts
{
    public class StoredPath
    {
        public PathFinder.Node.NLocation StartLocation;
        public PathFinder.Node.NLocation EndLocation;
        public Stack<Vector3> Path;
    }

    public class StoredPathDictionary
    {
        Dictionary<PathKey, StoredPath> CachedPaths;

        public StoredPathDictionary()
        {
            CachedPaths = new Dictionary<PathKey, StoredPath>();
        }
        struct PathKey
        {
            PathFinder.Node.NLocation Start;
            PathFinder.Node.NLocation End;
            int movableType;

            public PathKey(PathFinder.Node.NLocation start, PathFinder.Node.NLocation end, int movableType)
            {
                this.Start = start;
                this.End = end;
                this.movableType = movableType;
            }
        }

        
        public void AddToDictionary(PathFinder.Node.NLocation startLocation, PathFinder.Node.NLocation endLocation, Stack<Vector3> Path, int unitTypeId)
        {
            StoredPath spath = new StoredPath();
            spath.StartLocation = CloneNode(startLocation);
            spath.EndLocation = CloneNode(endLocation);
            spath.Path = CloneStack<Vector3>(Path);
            CachedPaths.Add(new PathKey(startLocation, endLocation, unitTypeId), spath);
        }

        public bool TryGetPath(PathFinder.Node.NLocation Start, PathFinder.Node.NLocation End, ref StoredPath path, IMovableUnit unit)
        {
            PathKey key = new PathKey(Start, End, unit.UnitTypeId);

            StoredPath pathOriginal;
            if (CachedPaths.TryGetValue(key, out pathOriginal))
            {
                path = ClonePath(pathOriginal);
                return true;
            }
            return false;
        }

        public List<StoredPath> GetAllPaths()
        {
            List<StoredPath> allPaths = new List<StoredPath>();
            foreach (var k in CachedPaths.Values)
            {
                allPaths.Add(ClonePath(k));
            }
            return allPaths;
        }

        StoredPath ClonePath(StoredPath original)
        {
            StoredPath clone = new StoredPath();
            //value type so ok to do this
            clone.StartLocation = original.StartLocation;
            clone.EndLocation = original.EndLocation;
            //stack is reference type so need to clone manually
            clone.Path = CloneStack<Vector3>(original.Path);
            return clone;
        }

        PathFinder.Node.NLocation CloneNode(PathFinder.Node.NLocation node)
        {
            return new PathFinder.Node.NLocation(node.x, node.y);
        }
        Stack<T> CloneStack<T>(Stack<T> stack)
        {
            return new Stack<T>(stack.Reverse<T>());
        }

        internal void RemoveEmpty()
        {
            List<StoredPathDictionary.PathKey> emptyKeys = new List<PathKey>();
            foreach (var path in this.CachedPaths.Keys)
            {
                if (this.CachedPaths[path].Path.Count == 0)
                    emptyKeys.Add(path);

            }
            foreach (var key in emptyKeys)
                this.CachedPaths.Remove(key);
        }
    }

    public interface IMovableUnit
    {
        int FactionId { get; }
        int UnitTypeId { get; } //different if should take different paths
        GameObject GameObject { get; }
    }

    public interface ILocalMap
    {
        int Width { get; set; }
        int Height { get; set; }
        bool IsBlocked(PathFinder.Node.NLocation end, IMovableUnit moveableUnit);
        int GetTerrainPenalty(IMovableUnit moveableUnit, PathFinder.Node.NLocation location);
        Vector3 LocationToVector(PathFinder.Node.NLocation loc);
        PathFinder.Node.NLocation VectorToLocation(Vector3 vec);
        bool ValidLocation(PathFinder.Node.NLocation node);
    }

    /// <summary>
    /// Implementation of A* pathfinding over anything which implements ILocalMap
    /// </summary>
    public class PathFinder
    {
        private ILocalMap map;
        private Node.NLocation goal;
        private Node.NLocation start;

        public Stack<Node.NLocation> LocationPath;
        private bool usepenalties = false;


        public static bool DEBUG = false;
        public int[,] FValueMap; //For debugging


        public PathFindOutcome Complete;
        int CutOffStepsPerUpdate = -1;
        int currentCutOff;

        Node currentnode;

        List<Node> openlist;
        Dictionary<UInt64, Node.NLocation> closedlist;
        List<Node> nodePool;
        List<Node> nodePoolInactive;
        int PoolSize = 200;
        public bool LastCacheMiss;
        IMovableUnit moveableUnit;
        public StoredPathDictionary cachedPaths;
        public Stack<Vector3> PathVectorStack;
        public List<Vector3> PathVectorList;

        public PathFinder(ILocalMap map)
        {
            cachedPaths = new StoredPathDictionary();
            this.map = map;

            openlist = new List<Node>();
            closedlist = new Dictionary<UInt64, Node.NLocation>();
            nodePool = new List<Node>();
            nodePoolInactive = new List<Node>();
            InitialiseNodePool();
            Reset();
        }

        void InitialiseNodePool()
        {
            for (int i = 0; i < PoolSize; i++)
                nodePool.Add(new Node(-1, -1));
        }

        Node GetPoolNode(int x, int y)
        {
            Node node = null;
            int count = nodePoolInactive.Count;
            for (int i = 0; i < count; i++)
            {
                if (!nodePoolInactive[i].Active)
                {
                    node = nodePoolInactive[i];
                    break;
                }
            }

            if (node == null)
            {
                node = new Node(x, y);
                nodePool.Add(node);
            }
            if (node != null)
            {
                nodePoolInactive.Remove(node);
            }
            node.Reset(x, y);
            return node;
        }

        private void Reset()
        {
            LocationPath = new Stack<Node.NLocation>();
            openlist.Clear();
            closedlist.Clear();
            for (int i = 0; i < nodePool.Count; i++)
                nodePool[i].Active = false;
            nodePoolInactive.AddRange(nodePool);
        }

        public bool DoesRouteExist(IMovableUnit moveableUnit, Node.NLocation startvec, Node.NLocation endvec)
        {
            var path = FindShortPath(moveableUnit, startvec, endvec);
            return path.Count != 0;
        }

        public Stack<Vector3> FindShortPath(IMovableUnit moveableUnit, Node.NLocation startvec, Node.NLocation endvec)
        {
            return FindShortPath(moveableUnit, startvec, endvec, true);
        }

        public void FindShortPathCoroutineStart(IMovableUnit moveableUnit, Node.NLocation startLoc, Node.NLocation endLoc, bool useTerrainPenalties, int iterationsPerUpdate,bool useCache = true, int cutOff = 20)
        {
            this.CutOffStepsPerUpdate = iterationsPerUpdate;
            this.start = startLoc;
            this.goal = endLoc;
            this.usepenalties = useTerrainPenalties;
            currentCutOff = CutOffStepsPerUpdate;
            FindShortPath(this.moveableUnit, this.start, this.goal, this.usepenalties, false, cutOff,iterationsPerUpdate);
        }

        public void FindShortPathCoroutineContinue()
        {
            ContinueSearch();
            if (Complete != PathFindOutcome.Incomplete)
                FinishSearch(moveableUnit, start, goal, false);
        }

        public void FindShortPathAsync(ref Stack<Vector3> path, IMovableUnit moveableUnit, Node.NLocation startLoc, Node.NLocation endLoc, bool useTerrainPenalties, bool useCache = false, int cutOff = -1)
        {
            path = FindShortPath(moveableUnit, startLoc, endLoc, useTerrainPenalties, useCache, cutOff);
        }

        void ContinueSearch()
        {
            currentnode = null;

            int iterations = 0;
            while (true)
            {
                if (CutOffStepsPerUpdate > 0 && iterations > CutOffStepsPerUpdate)
                    break;
                currentnode = FindNodeWithLowestScore();
                if (DEBUG)
                {
                    //display_open();
                    //display_closed();
                }
                if (currentnode == null) //No such path
                {
                    Complete =  PathFindOutcome.Fail;
                    break;
                }
                if (currentCutOff > 0 && CountStepsExceed(currentnode, currentCutOff))
                {
                    Complete = PathFindOutcome.ExceededLimit;
                    break;
                }
                MoveToClosed(currentnode);
                if (currentnode.Location.x == goal.x && currentnode.Location.y == goal.y)
                {
                    Complete = PathFindOutcome.Success;
                    break;
                }
                else
                    AddSurrounding(currentnode);
                iterations++;
            }

        }

        
        public Stack<Vector3> FindShortPath(IMovableUnit moveableUnit, Node.NLocation startvec, Node.NLocation endvec, bool useterrainpenalties, bool useCache = true, int cutOff = 20, int stepsPerUpdate = -1)
        {
            Complete = PathFindOutcome.Incomplete;
            LastCacheMiss = true;
            this.moveableUnit = moveableUnit;
            StoredPath storedPath = null;
            CutOffStepsPerUpdate = stepsPerUpdate;
            this.currentCutOff = cutOff;
            if (useCache && cachedPaths.TryGetPath(startvec, endvec, ref storedPath, moveableUnit))
            {
                LastCacheMiss = false;
                return storedPath.Path;
            }

            Reset();
            if (DEBUG)
            {
                FValueMap = new int[map.Width, map.Height];
            }

            start = startvec;
            goal = endvec;

            usepenalties = useterrainpenalties;

            Node startnode = GetPoolNode(start.x, start.y); 
            openlist.Add(startnode);
            ContinueSearch();
            if (Complete != PathFindOutcome.Success)
                return null;

            return FinishSearch(moveableUnit, startvec, endvec, useCache);
        }

        
        private Stack<Vector3> FinishSearch(IMovableUnit moveableUnit, Node.NLocation startvec, Node.NLocation endvec, bool useCache)
        {
            PathVectorStack = BuildVectorStack(currentnode);
            if (useCache)
                cachedPaths.AddToDictionary(startvec, endvec, PathVectorStack, moveableUnit.UnitTypeId);
            return PathVectorStack;
        }

        bool CountStepsExceed(Node node, int cutOff)
        {
            int count = 0;
            while (node.parent != null && count < cutOff)
            {
                count++;
                node = node.parent;
            }
            return count >= cutOff;
        }

        private Stack<Node.NLocation> BuildNodeStack(Node node)
        {
            Stack<Node.NLocation> stack = new Stack<Node.NLocation>();
            while (node != null)
            {
                Node.NLocation co = new Node.NLocation(node.Location.x, node.Location.y);
                stack.Push(co);
                node = node.parent;
            }
            return stack;
        }

        private Stack<Vector3> BuildVectorStack(Node node)
        {
            PathVectorList = new List<Vector3>();
            Stack<Vector3> stack = new Stack<Vector3>();
            while (node != null)
            {
                int x = stack.Count;
                Node.NLocation co = new Node.NLocation(node.Location.x, node.Location.y);
                LocationPath.Push(co);
                var vector = map.LocationToVector(co);
                stack.Push(vector);
                PathVectorList.Add(vector);
                node = node.parent;
            }
            return stack;
        }

        private void MoveToClosed(Node currentnode)
        {
            closedlist.Add(ConvertLocToInt64(currentnode.Location), currentnode.Location); //pack 2 32 bit ints into 64 bit int for dictionary with less GC alloc
            openlist.Remove(currentnode);
        }

        private Node FindNodeWithLowestScore()
        {
            openlist.Sort(delegate (Node n1, Node n2) { return n1.TotalScore.CompareTo(n2.TotalScore); });
            if (openlist.Count > 0)
                return openlist[0];
            else
                return null;
        }

        bool BlockedDiagonal(Node.NLocation current, Node.NLocation next)
        {
            if ((current.x == next.x) || current.y == next.y)
                return false;

            return map.IsBlocked(new Node.NLocation(current.x, next.y), this.moveableUnit) ||
                map.IsBlocked(new Node.NLocation(next.x, current.y), this.moveableUnit);


        }

        private void AddSurrounding(Node current)
        {
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (!(i == 0 && j == 0))
                    {
                        int nx = current.Location.x + i;
                        int ny = current.Location.y + j;
                        var next = new Node.NLocation(nx, ny);
                        if (!map.IsBlocked(next, this.moveableUnit) && !InClosed(next))
                        {
                            if (!BlockedDiagonal(current.Location, next))
                            {
                                Node node = GetPoolNode(nx, ny);
                                node.parent = current;
                                SetValues(node);
                                Node check = InOpen(next);

                                if (check == null)
                                {
                                    openlist.Add(node);
                                }
                                else if (node.DistanceToStart < check.DistanceToStart)
                                {
                                    check.parent = current;
                                    SetValues(check);
                                }
                            }

                        }
                    }

                }

            }
        }

        UInt64 ConvertLocToInt64(Node.NLocation Location)
        {
            return (((UInt64)Location.x) << 32) + (UInt64)Location.y;
        }
        private bool InClosed(Node.NLocation searchNode)
        {
            return closedlist.ContainsKey(ConvertLocToInt64(searchNode));
        }

        private Node InOpen(Node.NLocation searchNode)
        {
            foreach (Node node in openlist)
            {
                if (node.Location.x == searchNode.x && node.Location.y == searchNode.y)
                    return node;
            }
            return null;
        }

        private void SetValues(Node node)
        {
            float xdist = node.Location.x - node.parent.Location.x;
            float ydist = node.Location.y - node.parent.Location.y;

            if (((xdist * xdist) + (ydist * ydist)) == 1)
                node.DistanceToStart = node.parent.DistanceToStart + 10;
            else
                node.DistanceToStart = node.parent.DistanceToStart + 14;
            if (usepenalties)
                node.TerrainPenalty = map.GetTerrainPenalty(moveableUnit, node.Location);
            node.EstimatedDistanceToDestination = CalculateGoalDistanceHeuristic(node);
            node.TotalScore = node.DistanceToStart + node.EstimatedDistanceToDestination + node.TerrainPenalty;
            //if (DEBUG)
            //   MarkOnDebugMap(node.Location, node.f);
        }



        private float CalculateGoalDistanceHeuristic(Node node)
        {
            int x = node.Location.x - goal.x;
            int y = node.Location.y - goal.y;
            return Math.Abs(x) + Math.Abs(y); //faster approximation of SquareRoot((x * x)  + (y * y))
        }


        public class Node
        {
            public struct NLocation : IEquatable<NLocation>
            {
                public int x;
                public int y;

                public NLocation(int x, int y)
                {
                    this.x = x;
                    this.y = y;
                }

                public bool Equals(NLocation other)
                {
                    return this.x == other.x && this.y == other.y;
                }

                public override string ToString()
                {
                    return String.Format("({0}, {1})", x, y);
                }
            }

            public Node parent = null;
            public float TotalScore = 0; // lower is better
            public float DistanceToStart = 0;
            public float EstimatedDistanceToDestination = 0;
            public int TerrainPenalty = 0; //Terrain penalty modifier to have option of discouraging some nodes
            public NLocation Location;

            public bool Active; //for pooling

            public Node(int x, int y)
            {
                Location = new NLocation(x, y);
            }

            public void Reset(int x, int y)
            {
                Location.x = x;
                Location.y = y;
                parent = null;
                TotalScore = DistanceToStart = EstimatedDistanceToDestination = 0;
                TerrainPenalty = 0;
                Active = true;
            }
        }

        public enum PathFindOutcome { Incomplete, Fail, Success, ExceededLimit };

        #region Debugging functions

        //private void MarkOnDebugMap(Node.NLocation location, decimal p)
        //{
        //    m_fvaluemap[location.x, location.y] = p;
        //}

        private void DisplayOpen()
        {
            Debug.Log("Open List");
            ShowList(openlist);

        }

        private void DisplayClosed()
        {
            //Debug.Log("Closed List");
            //ShowList(closedlist);

        }
        private void ShowList(List<Node> list)
        {
            foreach (Node nod in list)
            {
                Debug.Log("Location " + "(" + nod.Location.x + "," + nod.Location.y + ")"
                  + " f g h = " + nod.TotalScore + " " + nod.DistanceToStart + " " + nod.EstimatedDistanceToDestination);
            }
        }

        public void InvalidateNullCache()
        {
            this.cachedPaths.RemoveEmpty();
        }
        #endregion
    }
}
