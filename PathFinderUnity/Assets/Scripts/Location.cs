﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public enum Direction { Up, Down, Left, Right };
    public enum TileType {None, Mountain,Tree,Water,Wood,Ore,Fertile }
    public class TileInfo
    {
        public TileType TileType;
        public float[] TileChances;
        public float Perlin;
        public bool Done;
        public TileInfo()
        {
            TileChances = new float[Enum.GetNames(typeof(TileType)).Length];
            Done = false;
        }
        public void SetChance(TileType tile, float chance)
        {
            TileChances[(int)tile] = chance;
        }

       
    }
    public class Location
    {
        static Location nullLocation = new Location() { x = -1, y = -1 };
        
        public GameObject HiddenTile;
        public TileInfo TileInfo;
        public int WaterAccess;
        public int MarketAccess;
        public int HappinessScore;
             
        public int x;
        public int y;
        public bool blocked;
        bool fertile;
        bool wood;
        bool ore;

        public void SetTerrain(bool blocked, bool fertile, bool wood, bool ore)
        {
            this.blocked = blocked;
            this.wood = wood;
            this.fertile = fertile;
            this.ore = ore;

        }
        public Location()
        {
            TileInfo = new TileInfo();
        }

        internal static Location NullLocation()
        {
            return nullLocation;
        }


        public bool IsNullLocation()
        {
            return this.x < 0 && this.y < 0;
        }

        public override string ToString()
        {
            return string.Format("({0},{1}) ", x, y);
        }

        public bool Fertile { get { return fertile; } }
        public bool Wood { get { return wood; } }

        public int DangerPenalty { get; set; }
        public bool Ore { get { return ore; } }
        internal bool IsBlocked()
        {
            if (IsNullLocation())
                return true;

            return blocked;
        }

        public PathFinder.Node.NLocation ToPathFinderNode()
        {
            return new PathFinder.Node.NLocation(x, y);
        }

        internal void UpdateTerrainForTileInfo()
        {
            if (this.TileInfo == null)
                return;
            bool fertile = TileInfo.TileType == TileType.Fertile;
            bool ore = TileInfo.TileType == TileType.Ore;
            bool wood = TileInfo.TileType == TileType.Wood;
            this.SetTerrain(false, fertile, wood, ore);
        }

        public bool HasTileType(TileType tile)
        {
            if (IsNullLocation())
                return false;
            return TileInfo.TileType == tile;
        }

        internal void UpdateBlocked()
        {
            if (this.TileInfo.TileType == TileType.Mountain || this.TileInfo.TileType == TileType.Water || this.TileInfo.TileType == TileType.Tree)
                blocked = true;
            else
                blocked = false;
        }
    }
}
