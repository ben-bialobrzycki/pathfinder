﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class TestLevelManager : MonoBehaviour
{
    public struct ParamValues
    {
        public string LabelText;
        public float MinValue;
        public float MaxValue;
        public float DefaultValue;
        public bool WholeNumbers;
    }

    [Serializable]
    public struct ParamControls
    {
        public Text TxtLabel;
        public Text TxtValue;
        public Slider SliderControl;
    }


    public GameObject TilePrefab;
    public GameObject WallTilePrefab;
    public GameObject MarkedPathPrefab;

    public GameObject pnlWait;


    public GameObject GrassTilePrefab;

    public GameObject WaterLowTilePrefab;
    public GameObject WaterHighTilePrefab;

    public GameObject WoodLowTilePrefab;
    public GameObject WoodHighTilePrefab;


    public GameObject MountainLowTilePrefab;
    public GameObject MountainHighTilePrefab;


    Camera mainCamera;
    TestLevelMap randomMap;
    DungeonLevelMap dungeonLevelMap;

    ILocalMap localMap;
    PathFinder pathFinder;
    List<GameObject> tiles;

    Vector3 startPath;
    Vector3 endPath;
    TestUnit moveableUnit;
    public Dropdown OptionDropDown;
    public Dropdown MapStyleDropDown;


    public ParamValues Param1;
    public ParamValues Param2;
    public ParamValues Param3;


    public ParamControls ParamControls1;
    public ParamControls ParamControls2;
    public ParamControls ParamControls3;

    public Text TxtErrorMessage;
    int frameCounter = 0;
    enum PathOption { Coroutine, AsyncTask, MainThread }

    PathOption selectedOption = PathOption.Coroutine;


    public enum MapType { RandomMap, DungeonMap, PerlinMap }

    MapType mapType = MapType.DungeonMap;

    void ShowMessage(string msg)
    {
        TxtErrorMessage.text = msg;
    }

    void ClearErrorMessage()
    {
        ShowMessage("");
    }


    IEnumerator RunGenerateMap()
    {
        yield return new WaitForSeconds(0.1f);
        BuildMap();

    }

    void BuildMap()
    {
        mapType = (MapType)MapStyleDropDown.value;
        ClearTiles();

        //map.Generate();
        if (mapType == MapType.RandomMap)
        {
            randomMap = new TestLevelMap();
            randomMap.GenerateRandomMap();
            GenerateTiles();
            localMap = randomMap;
        }
        else if (mapType == MapType.DungeonMap)
        {
            dungeonLevelMap = new DungeonLevelMap();
            int roomCount = (int)ParamControls1.SliderControl.value;
            int roomSizeMin = (int)ParamControls2.SliderControl.value;
            int roomSizeMax = (int)ParamControls3.SliderControl.value;
            if (roomSizeMin > roomSizeMax)
            {
                ShowMessage("Min Room size must be above max room size");
                pnlWait.SetActive(false);
                return;
            }
            dungeonLevelMap.BuildDungeon(roomCount, roomSizeMin, roomSizeMax);
            DisplayDungeonMap(dungeonLevelMap);
            localMap = dungeonLevelMap;
        }
        else if (mapType == MapType.PerlinMap)
        {
            LevelGenerator levelGenerator = new LevelGenerator();
            float scale = ParamControls1.SliderControl.value;
            float thresholdTier1 = ParamControls2.SliderControl.value;
            float thresholdTier2 = ParamControls3.SliderControl.value;

            if (thresholdTier1 > thresholdTier2)
            {
                ShowMessage("Threshold 1 must be below Threshold 2");
                pnlWait.SetActive(false);
                return;
            }
            levelGenerator.GenerateLevel(100, 100, scale, thresholdTier1, thresholdTier2);
            this.localMap = levelGenerator.WorldMap;
            DisplayPerlinMap(levelGenerator.WorldMap);
        }

        pathFinder = new PathFinder(localMap);
        pnlWait.SetActive(false);
    }

    public void GenerateMap()
    {
        StopAllCoroutines();
        pnlWait.SetActive(true);
        StartCoroutine(RunGenerateMap());

    }


    GameObject PerlinMapTilePrefabForTileType(TileType tileType)
    {
        switch (tileType)
        {
            case TileType.Wood: return WoodLowTilePrefab;
            case TileType.Tree: return WoodHighTilePrefab;
            case TileType.Ore: return MountainLowTilePrefab;
            case TileType.Mountain: return MountainHighTilePrefab;
            case TileType.Fertile: return WaterLowTilePrefab;
            case TileType.Water: return WaterHighTilePrefab;

        }
        return GrassTilePrefab;
    }

    private void DisplayPerlinMap(WorldMap worldMap)
    {

        for (int x = 0; x < worldMap.Width; x++)
        {
            for (int y = 0; y < worldMap.Height; y++)
            {
                Location loc = worldMap.GetLocation(x, y);
                if (loc.TileInfo.Done)
                    continue;

                GameObject prefab = PerlinMapTilePrefabForTileType(loc.TileInfo.TileType);
                SetTile(loc.x, loc.y, prefab);

            }
        }

    }

    void SetTile(int x, int y, GameObject tilePrefab)
    {
        Vector3 position = localMap.LocationToVector(new PathFinder.Node.NLocation(x, y));
        GameObject go = GameObject.Instantiate(tilePrefab, position, Quaternion.identity);
        tiles.Add(go);
    }

    void DisplayDungeonMap(DungeonLevelMap levelMap)
    {
        levelMap.RunOverAllCells((x, y) =>
        {
            if (levelMap.grid[y, x].HasLayer(DungeonLevelMap.CellType.Ground))
            {
                SetTile(x, y, TilePrefab);
            }

            if (levelMap.grid[y, x].HasLayer(DungeonLevelMap.CellType.Wall))
            {
                SetTile(x, y, WallTilePrefab);
            }

        });
    }


    public void ChangeOption()
    {
        int option = OptionDropDown.value;
        selectedOption = (PathOption)option;
    }

    void GenerateTiles()
    {
        for (int y = 0; y < randomMap.map.GetLength(0); y++)
        {
            for (int x = 0; x < randomMap.map.GetLength(1); x++)
            {
                int tile = randomMap.map[y, x];
                Vector3 position = randomMap.LocationToVector(new PathFinder.Node.NLocation(x, y));
                if (tile == 0)
                    tiles.Add(GameObject.Instantiate(TilePrefab, position, Quaternion.identity));
                if (tile == 1)
                    tiles.Add(GameObject.Instantiate(WallTilePrefab, position, Quaternion.identity));
            }
        }
    }

    void ClearTiles()
    {
        foreach (var go in tiles)
            GameObject.Destroy(go);
        tiles.Clear();
    }

    void Start()
    {
        tiles = new List<GameObject>();
        mainCamera = Camera.main;
        GenerateMap();
        moveableUnit = new TestUnit();
        MapStyleDropDown.onValueChanged.AddListener(MapStyleChanged);
        ParamControls1.SliderControl.onValueChanged.AddListener((value) => { ShowValue(value, ParamControls1.SliderControl, ParamControls1.TxtValue); });
        ParamControls2.SliderControl.onValueChanged.AddListener((value) => { ShowValue(value, ParamControls2.SliderControl, ParamControls2.TxtValue); });
        ParamControls3.SliderControl.onValueChanged.AddListener((value) => { ShowValue(value, ParamControls3.SliderControl, ParamControls3.TxtValue); });
        MapStyleChanged(0);
        pnlWait.SetActive(false);
    }

    void ShowValue(float value, Slider slider, Text text)
    {
        if (slider.wholeNumbers)
            text.text = ((int)value).ToString();
        else
            text.text = value.ToString("0.00");
    }


    void MapStyleChanged(int change) //Unity DropDown listener requires this change param even though its not used
    {
        MapType mapType = (MapType)MapStyleDropDown.value;

        switch (mapType)
        {
            case MapType.RandomMap:
                EnableParamAllControls(false);
                break;
            case MapType.DungeonMap:
                EnableParamAllControls(true);
                SetParamsForDungeonGeneration();
                break;
            case MapType.PerlinMap:
                EnableParamAllControls(true);
                SetParamsForPerlinMapGeneration();
                break;
        }

    }

    private void EnableParamAllControls(bool enableParams)
    {
        EnableParamControl(ParamControls1, enableParams);
        EnableParamControl(ParamControls2, enableParams);
        EnableParamControl(ParamControls3, enableParams);
    }

    void EnableParamControl(ParamControls paramControls, bool enable)
    {
        paramControls.TxtLabel.enabled = paramControls.TxtValue.enabled = paramControls.SliderControl.enabled = enable;
    }

    private void SetParamsForDungeonGeneration()
    {
        ParamValues values1 = new ParamValues() { LabelText = "Rooms", MinValue = 5, MaxValue = 40, DefaultValue = 12, WholeNumbers = true };
        ParamValues values2 = new ParamValues() { LabelText = "Room Size Min", MinValue = 5, MaxValue = 20, DefaultValue = 10, WholeNumbers = true };
        ParamValues values3 = new ParamValues() { LabelText = "Room Size Max", MinValue = 10, MaxValue = 30, DefaultValue = 15, WholeNumbers = true };

        SetParamControlsForValues(values1, values2, values3);

    }

    private void SetParamControlsForValues(ParamValues values1, ParamValues values2, ParamValues values3)
    {
        SetParamValuesForControl(values1, ParamControls1);
        SetParamValuesForControl(values2, ParamControls2);
        SetParamValuesForControl(values3, ParamControls3);
    }

    private void SetParamsForPerlinMapGeneration()
    {
        ParamValues values1 = new ParamValues() { LabelText = "Scale", MinValue = 1, MaxValue = 10, DefaultValue = 4.0f, WholeNumbers = false };
        ParamValues values2 = new ParamValues() { LabelText = "Tier 1 Threshold", MinValue = 0, MaxValue = 1, DefaultValue = 0.2f, WholeNumbers = false };
        ParamValues values3 = new ParamValues() { LabelText = "Tier 2 Threshold", MinValue = 0, MaxValue = 1, DefaultValue = 0.7f, WholeNumbers = false };

        SetParamControlsForValues(values1, values2, values3);
    }

    void SetParamValuesForControl(ParamValues values, ParamControls control)
    {
        control.SliderControl.wholeNumbers = values.WholeNumbers;
        control.SliderControl.minValue = values.MinValue;
        control.SliderControl.maxValue = values.MaxValue;
        control.SliderControl.value = values.DefaultValue;
        control.SliderControl.onValueChanged.Invoke(values.DefaultValue);
        control.TxtLabel.text = values.LabelText;

    }

    void Update()
    {
        if (pathTask != null)
            frameCounter++;

        if (Input.GetKeyDown(KeyCode.G))
            GenerateMap();

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 worldPos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            startPath = worldPos;
            ShowMessage(String.Format("Set start as {0}", localMap.VectorToLocation(startPath)));
        }

        if (Input.GetMouseButtonDown(1))
        {
            if (pathTask != null)
                return;
            Vector3 worldPos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            endPath = worldPos;
            ShowMessage(String.Format("Set end as {0}", localMap.VectorToLocation(endPath)));

            var startNode = localMap.VectorToLocation(startPath);
            var endNode = localMap.VectorToLocation(endPath);

            if (!localMap.ValidLocation(startNode) || !localMap.ValidLocation(endNode))
            {
                ShowMessage(String.Format("start {0} to end {1} is not valid", startNode, endNode));
                return;
            }

            if (selectedOption == PathOption.Coroutine)
            {
                StopAllCoroutines();
                StartCoroutine(RunPathFindCoroutine(startNode, endNode));
            }
            else if (selectedOption == PathOption.AsyncTask)
            {
                if (pathTask != null && !pathTask.IsCompleted)
                    return;
                RunFindPathAsTask(startNode, endNode);
            }
            else if (selectedOption == PathOption.MainThread)
                RunFindPathMainThread(startNode, endNode);

        }

        if (selectedOption == PathOption.AsyncTask && pathTask != null && frameCounter > 100)
        {
            ShowMessage("Path took too long forcing wait");
            pathTask.Wait();
        }
        if (selectedOption == PathOption.AsyncTask && pathTask != null && pathTask.IsCompleted)
        {
            ShowMessage(string.Format("Finished Find Path in {0} frames", frameCounter));
            DisplayPath(path);
            pathTask = null;
        }
    }

    Task pathTask;
    Stack<Vector3> path;
    void RunFindPathAsTask(PathFinder.Node.NLocation start, PathFinder.Node.NLocation end)
    {
        frameCounter = 0;
        ShowMessage(string.Format("starting find path {0} to end {1}", start, end));
        pathTask = new Task(() => pathFinder.FindShortPathAsync(ref path, moveableUnit, start, end, false, false, -1));
        pathTask.Start();
    }

    int frameCount;
    IEnumerator RunPathFindCoroutine(PathFinder.Node.NLocation start, PathFinder.Node.NLocation end)
    {
        frameCount = 1;
        pathFinder.FindShortPathCoroutineStart(moveableUnit, start, end, false, 50, false, -1);

        while (pathFinder.Complete == PathFinder.PathFindOutcome.Incomplete)
        {
            yield return null;
            pathFinder.FindShortPathCoroutineContinue();
            frameCount++;
        }
        ShowMessage(string.Format("Took {0} Frames as coroutine", frameCount));
        DisplayPath(pathFinder.PathVectorStack);
    }

    void RunFindPathMainThread(PathFinder.Node.NLocation start, PathFinder.Node.NLocation end)
    {
        frameCounter = 0;
        ShowMessage(string.Format("starting find path {0} to end {1}", start, end));
        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        sw.Start();
        var path = pathFinder.FindShortPath(moveableUnit, start, end, false, false, -1, -1);
        sw.Stop();
        string msg = String.Format("Took {0}ms to complete search", sw.ElapsedMilliseconds);
        ShowMessage(msg);
        DisplayPath(path);
    }

    void DisplayPath(Stack<Vector3> path)
    {
        if (path == null || path.Count <= 0)
        {
            ShowMessage(string.Format("No Path found for {0} to {1}", startPath, endPath));
            return;
        }
        foreach (var pathStep in path)
        {
            tiles.Add(GameObject.Instantiate(MarkedPathPrefab, pathStep, Quaternion.identity));
        }
    }
}

public class TestUnit : IMovableUnit
{
    public int FactionId
    {
        get
        {
            return 0;
        }
    }

    public int UnitTypeId { get; set; }

    public bool NeedRoad { get; set; }

    public GameObject GameObject { get; set; }
}
