﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    [Serializable]
    public class MapSettings
    {
        public int Width;
        public int Height;
    //    public MapOptions.MapSize Size;
    }
    //[Serializable]
    //public class MapDifficultySettings
    //{
    //    public int SpawnCount;
    //    public LevelResource Resources;
    //    public float MinSpawnDistance = 10.0f;

    //    public int initialSpawnDaysOn;//= 7;
    //    //int maxSpawnDaysOn = 14;
    //    public int initialSpawnDaysInterval;// = 14;
    //    public int initialSpawnDelay;
    //    //int minSpawnDaysInterval = 5;
    //}
    class LevelGenerator
    {
        public WorldMap WorldMap;
        int Height = 100;
        int Width = 100;
        public static int DefaultWidth = 40;
        public static int DefaultHeight = 30;
        public float TreeThreshold = 0.7f;
        public float MountainThreshold = 0.7f;
        public float WaterThreshold = 0.7f;
        public float InitialPerlinThreshold = 0.7f;//0.62f;
        internal void GenerateLevel(int width, int height, float scale, float tier1Threshold, float tier2Threshold )
        {
            this.Width = width;
            this.Height = height;
            TreeThreshold = MountainThreshold = WaterThreshold = tier2Threshold;
            BuildRandomMap(scale, tier1Threshold);
        }

        float PerlinThreshold;
        public float Scale = 2.5f;//0.6f;
        float PerlinWall(int x, int y)
        {

            float px = ((float)x / Width) * Scale; // (UnityEngine.Random.Range(0,1) * Scale ) + Scale * x;
            float py = ((float)y / Height) * Scale;// (UnityEngine.Random.Range(0, 1) * Scale) + Scale * y;
            float noise = Mathf.PerlinNoise(px + xOrig, py + yOrig);
            return noise;

        }
        public float[,] noiseMap;
        float xOrig;
        float yOrig;
        public void BuildRandomMap(float scale, float threshold)
        {

            this.Scale = scale;
            this.PerlinThreshold = threshold;
            xOrig = UnityEngine.Random.Range(0.0f, 1.0f) * Scale;
            yOrig = UnityEngine.Random.Range(0.0f, 1.0f) * Scale;

            this.WorldMap = new WorldMap();
            this.WorldMap.InitialiseMap(Width, Height, null);

            noiseMap = new float[Width, Height];

            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    TileType[] tileTypes = { TileType.Fertile, TileType.Ore, TileType.Wood }; //(TileType[])Enum.GetValues(typeof(TileType));
                    Location loc = WorldMap.GetLocation(x, y);
                    foreach (var tt in tileTypes)
                    {
                        int offset = Width * (int)tt;
                        float noise = PerlinWall(x + offset, y);
                        loc.TileInfo.SetChance(tt, noise);
                    }
                    SetTile(loc.TileInfo,PerlinThreshold);
                    loc.UpdateBlocked();
                 
                }
            }
        }

        internal void SetTile(TileInfo tileInfo, float threshold)
        {
            float max = -1;
            int selected = -1;
            for (int i = 0; i < tileInfo.TileChances.Length; i++)
            {
                if (max < tileInfo.TileChances[i])
                {
                    max = tileInfo.TileChances[i];
                    selected = i;
                }
            }
            if (max < threshold)
                tileInfo.TileType = TileType.None;
            else
                tileInfo.TileType = (TileType)selected;
            tileInfo.Perlin = max;
            if (tileInfo.TileType == TileType.Fertile && tileInfo.Perlin > WaterThreshold)
                tileInfo.TileType = TileType.Water;
            if (tileInfo.TileType == TileType.Wood && tileInfo.Perlin > TreeThreshold)
                tileInfo.TileType = TileType.Tree;
            if (tileInfo.TileType == TileType.Ore && tileInfo.Perlin > MountainThreshold)
                tileInfo.TileType = TileType.Mountain;
            
        }
    }
}
