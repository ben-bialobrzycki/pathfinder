﻿using Assets.Scripts;
using UnityEngine;

public class TestLevelMap : ILocalMap
{
    public int[,] map;
    public int Width
    {
        get { return map.GetLength(1); }
        set{; } 
       
    }

    public void Generate()
    {
        map = new int[,]
        {
            {0,0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,1,0,0,0,0,0,0,0 },
            {0,0,0,1,0,0,1,0,1,0,0 },
            {0,0,0,1,0,0,1,0,0,0,0 },
            {0,0,0,0,0,1,1,0,1,0,0 },
            {0,0,0,1,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,1,0,0 },
            {0,0,0,0,0,0,0,0,0,0,0 }
        };

        
        
    }

    public float WallChance = 0.3f;
    public void GenerateRandomMap()
    {
        map = new int[100, 100];
        for (int y=0; y < map.GetLength(0);y++)
        {
            for (int x=0; x < map.GetLength(1);x++)
            {
                if (UnityEngine.Random.Range(0, 1.0f) < WallChance)
                    map[y, x] = 1;
                else
                    map[y, x] = 0;

            }
        }
    }
    public int Height { get { return map.GetLength(0); } set{; } }

    public int GetTerrainPenalty(IMovableUnit moveableUnit, PathFinder.Node.NLocation location)
    {
        return 0;
    }

    public bool IsBlocked(PathFinder.Node.NLocation end, IMovableUnit moveableUnit)
    {
        if (!ValidLocation(end))
            return true;
        return map[end.y,end.x] == 1;
    }

    public Vector3 LocationToVector(PathFinder.Node.NLocation loc)
    {
        return new Vector3(loc.x + 0.5f, loc.y + 0.5f);
    }

    public bool ValidLocation(PathFinder.Node.NLocation node)
    {
        if (node.x < 0 || node.y < 0 || node.x >= Width || node.y >= Height)
            return false;
        return true;
    }
    public PathFinder.Node.NLocation VectorToLocation(Vector3 vec)
    {
        int x = Mathf.RoundToInt(vec.x - 0.5f);
        int y = Mathf.RoundToInt(vec.y - 0.5f);
        return new PathFinder.Node.NLocation(x, y);
    }
}
