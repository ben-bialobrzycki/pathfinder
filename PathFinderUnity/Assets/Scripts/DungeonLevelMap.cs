﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class DungeonLevelMap : ILocalMap
{
    public struct MapCell
    {
        public int X;
        public int Y;
        // public GenTileBehaviour tileBehaviour;
        public int LayerMask;

        public bool HasLayer(CellType cellType)
        {
            return (LayerMask & (int)cellType) == (int)cellType; //return (LayerMask & (int)cellType) == (int)cellType;
                                                                 //return LayerMask == (int)cellType;
        }
    }

    public enum CellType { None = 1, Ground = 4, Wall = 8, Debug = 16, Furniture = 32, Column = 64, Plant = 128 }

    public enum WallType { None, TopLeft, Top, TopRight, Right, BottomRight, Bottom, BottomLeft, Left, InnerBottomRight, InnerBottomLeft, InnerTopRight, InnerTopLeft }

    public int width = 100;//30;
    public int height = 100;//30;

    public DungeonLevelMap()
    {
        //BuildFurniturePatterns();
    }
    public MapCell[,] grid;
    int roomCount;

    int roomWidthMin;
    int roomWidthMax;


    int roomHeightMin;
    int roomHeightMax;

    Cell CentralRoom;
    public List<Cell> RoomCentres;

    public int Width
    {
        get
        {
            return width;
        }

        set
        {
            width = value;
        }
    }

    public void SetBlock(Cell startCell, int[,] testBlock)
    {
        testBlock = FlipBlock(testBlock);
        for (int y = 0; y < testBlock.GetLength(0); y++)
        {
            for (int x = 0; x < testBlock.GetLength(1); x++)
            {
                Cell gridCell = new Cell(startCell.X + x, startCell.Y + y);
                if (OffMap(gridCell))
                    continue;
                grid[gridCell.Y, gridCell.X].LayerMask = testBlock[y, x];
            }
        }
    }

    public CellType GetCellType(int x, int y)
    {
        return (CellType)grid[y, x].LayerMask;
    }

    public bool HasLayer(int x, int y, CellType cellType)
    {
        return grid[y, x].HasLayer(cellType);
    }
    public static int[,] FlipBlock(int[,] block)
    {
        int[,] flipped = new int[block.GetLength(0), block.GetLength(1)];

        for (int y = 0; y < block.GetLength(0); y++)
        {
            for (int x = 0; x < block.GetLength(1); x++)
            {
                flipped[y, x] = block[(block.GetLength(0) - 1) - y, x];
            }
        }
        return flipped;
    }

    public void SetCell(Cell cell, CellType cellType)
    {
        grid[cell.Y, cell.X].LayerMask = (int)cellType;
    }
    public int Height { get { return height; } set { height = value; } }

    public void BuildDungeon(int roomCount, int roomMinSize, int roomMaxSize)
    {
        this.roomCount = roomCount;
        this.roomHeightMin = this.roomWidthMin = roomMinSize;
        this.roomHeightMax = this.roomWidthMax = roomMaxSize;
        ReCreateGrid();
        PlaceRooms();
        BuildCorridors();
        PlaceWalls();
        CullOddWallsRepeat();
    }

    public void CullOddWallsRepeat()
    {
        int limit = 10;
        while (limit > 0)
        {
            int cullCount = CullOddWalls();
            Debug.LogFormat("Inner Wall Count Cull {0}", cullCount);
            if (cullCount == 0)
                break;
            limit--;
        }
    }
    public bool ValidateDungeon()
    {
        var playerCell = RoomCentres[0];

        PathFinder pathFinder = new PathFinder(this);
        for (int i = 1; i < RoomCentres.Count; i++)
        {
            MockUnit unit = new MockUnit();
            Cell end = RoomCentres[i];
            Stack<Vector3> path = pathFinder.FindShortPath(unit, new PathFinder.Node.NLocation(playerCell.X, playerCell.Y), new PathFinder.Node.NLocation(end.X, end.Y), true, false, 1000);
            if (path == null || path.Count == 0)
                return false;
        }
        return true;

    }

    void ReCreateGrid()
    {
        grid = new MapCell[height, width];
        ClearAllCells();
    }
    public void SetSize(int width, int height)
    {
        this.width = width;
        this.height = height;
        ReCreateGrid();
    }

    public void PlaceWalls()
    {
        RunOverAllCells((x, y) =>
        {
            if (x < 0 || x > width - 1)
                return;
            if (y < 0 || x > height - 1)
                return;
            if (grid[y, x].LayerMask == (int)CellType.None)
            {
                for (int i = x - 1; i <= x + 1; i++)
                {
                    if (i < 0 || i >= width)
                        continue;
                    for (int j = y - 1; j <= y + 1; j++)
                    {
                        if (j < 0 || j >= height)
                            continue;
                        if (grid[j, i].LayerMask == (int)CellType.Ground)
                        {
                            grid[y, x].LayerMask = (int)CellType.Wall;
                            return;
                        }
                    }
                }
            }
        });
    }

    class MockUnit : IMovableUnit
    {
        public int FactionId
        {
            get
            {
                return 0;
            }
        }

        public int UnitTypeId { get { return 0; } }

        public bool NeedRoad { get { return false; } }

        public GameObject GameObject { get { return null; } }
    }

    public void BuildCorridors()
    {

        for (int i = 0; i < RoomCentres.Count; i++)
        {
            PathFinder pathFinder = new PathFinder(this);
            MockUnit unit = new MockUnit();
            Cell start = CentralRoom; 
            Cell end = RoomCentres[i];
            Stack<Vector3> path = pathFinder.FindShortPath(unit, new PathFinder.Node.NLocation(start.X, start.Y), new PathFinder.Node.NLocation(end.X, end.Y), true, false, 1000);
            if (path == null || path.Count == 0)
            {
                Debug.LogFormat("No Path found from {0} to {1}", start, end);
            }

            BlockOutPath(path);
        }
    }


    void BlockOutPath(Stack<Vector3> path)
    {
        if (path == null)
            return;
        while (path.Count > 0)
        {
            Vector3 vec = path.Pop();
            var nlocation = VectorToLocation(vec);
            Cell cell = new Cell(nlocation.x, nlocation.y);

            RunOverCells((x, y) =>
            {
                if (OffMap(new Cell(x, y)))
                    return;
                grid[y, x].LayerMask = (int)CellType.Ground;
            },
             new Cell(cell.X - 1, cell.Y - 1), new Cell(cell.X, cell.Y), true);
            
        }
    }

    RoomSize RandomRoomSize()
    {
        int roomSizeWidth = UnityEngine.Random.Range(roomWidthMin, roomWidthMax);
        int roomSizeHeight = UnityEngine.Random.Range(roomHeightMin, roomHeightMax);
        return new RoomSize(roomSizeWidth, roomSizeHeight);
    }

    Cell RandomCell()
    {
        int xcorner = UnityEngine.Random.Range(2, width - 2);
        int ycorner = UnityEngine.Random.Range(2, height - 2);
        return new Cell(xcorner, ycorner);
    }

    int Clamp(int value, int min, int max)
    {
        if (value < min)
            return min;
        if (value > max)
            return max;
        return value;
    }

    Cell RoomCentre(Cell topLeftCorner, RoomSize size)
    {
        Cell centre = new Cell(topLeftCorner.X + size.Width / 2, topLeftCorner.Y + size.Height / 2);
        centre.X = Clamp(centre.X, 0, width - 1);
        centre.Y = Clamp(centre.Y, 0, height - 1);
        return centre;
    }

    private void PlaceRooms()
    {
        RoomCentres = new List<Cell>();

        Cell mainRoomCorner = RoomCentre(new Cell(0, 0), new RoomSize(width, height));
        RoomSize mainRoomSize = RandomRoomSize();
        BlockOutRoom(mainRoomCorner, mainRoomSize);

        CentralRoom = RoomCentre(mainRoomCorner, mainRoomSize);

        for (int i = 0; i < roomCount; i++)
        {
            RoomSize roomSize = RandomRoomSize();
            Cell start = RandomCell();
            BlockOutRoom(start, roomSize);
            RoomCentres.Add(start);
        }
    }

    void GetSurroundingCells(Cell centre, ref int[] surround)
    {
        Cell start = new Cell(centre.X - 1, centre.Y - 1);
        Cell end = new Cell(centre.X + 1, centre.Y + 1);

        int cellSurround = 0;
        for (int x = start.X; x <= end.X; x++)
        {
            for (int y = start.Y; y <= end.Y; y++)
            {
                if (centre.X == x && centre.Y == y)
                    continue;

                if (OffMap(new Cell(x, y)))
                    surround[cellSurround] = (int)CellType.None;
                else
                    surround[cellSurround] = grid[y, x].LayerMask;
                cellSurround++;
            }
        }

    }

    public int CullOddWalls()
    {
        int[] surround = new int[8];
        int cullCount = 0;
        RunOverAllCells((x, y) =>
        {
            if (grid[y, x].LayerMask == (int)CellType.Wall)
            {
                for (int i = 0; i < surround.Length; i++)
                    surround[i] = 0;

                GetSurroundingCells(new Cell(x, y), ref surround);
                bool foundOne = false;
                for (int i = 0; i < surround.Length; i++)
                {
                    if (surround[i] == (int)CellType.None)
                    {
                        foundOne = true;
                        break;
                    }

                }
                if (!foundOne)
                {
                    grid[y, x].LayerMask = (int)CellType.Ground;
                        cullCount++;
                }
                }

        });
        return cullCount;
    }
    private void BlockOutRoom(Cell start, RoomSize roomSize)
    {
        Cell end = new Cell(start.X + roomSize.Width, start.Y + roomSize.Height);
        RunOverCells((x, y) =>
        {
            if (OffMap(new Cell(x, y)))
                return;
            grid[y, x].LayerMask = (int)CellType.Ground;

        }, start, end, true);
    }

    public void ClearAllCells()
    {
        RunOverAllCells((x, y) => { grid[y, x].LayerMask = (int)CellType.None; });
    }

    public void RunOverAllCells(ApplyToCell cellFunc)
    {
        RunOverCells(cellFunc, StartCell(), EndCell());
    }


    public void RunOverCells(ApplyToCell cellFunc, Cell start, Cell end, bool leaveBorder = false)
    {
        for (int i = start.X; i <= end.X; i++)
        {

            if ((i >= width || i < 0) || (leaveBorder && i >= width - 2))
                continue;
            for (int j = start.Y; j <= end.Y; j++)
            {
                if (j >= height || j < 0 || (leaveBorder && j >= height - 2))
                    continue;
                cellFunc(i, j);

            }
        }

    }

    Cell StartCell()
    {
        return new Cell(0, 0);
    }

    Cell EndCell()
    {
        return new Cell(width, height);
    }


    
    void MatchPattern()
    {

    }

    int[,] top =
        {
            { 0, 0, 0 },
            { 8, 8, 8 },
            { 0, 4, 0 }
        };

    int[,] topleft =
    {
            { 0, 0, 0 },
            { 0, 8, 8 },
            { 0, 8, 4 }
        };
    int[,] topright =
      {
            { 0, 0, 0 },
            { 8, 8, 0 },
            { 4, 8, 0 }
        };

    int[,] left =
     {
            { 0, 8, 0 },
            { 0, 8, 4 },
            { 0, 8, 0 }
        };

    int[,] right =
     {
            { 0, 8, 0 },
            { 4, 8, 0 },
            { 0, 8, 0 }
        };

    int[,] bottom =
     {
            { 0, 4, 0 },
            { 8, 8, 8 },
            { 0, 0, 0 }
     };

    int[,] bottomleft =
    {
            { 0, 8, 4 },
            { 0, 8, 8 },
            { 0, 0, 0 }
     };

    int[,] bottomright =
   {
            { 4, 8, 0 },
            { 8, 8, 0 },
            { 0, 0, 0 }
     };


    int[,] innerbottomright =
   {
            { 0, 8, 1 },
            { 4, 8, 8 },
            { 4, 4, 0 }
     };

    int[,] innerbottomleft =
 {
            { 1, 8, 0 },
            { 8, 8, 4 },
            { 0, 4, 0 }
     };

    int[,] innertopright =
{
            { 0, 4, 0 },
            { 4, 8, 8 },
            { 0, 8, 1 }
     };

    int[,] innertopleft =
 {
            { 0, 4, 0 },
            { 8, 8, 4 },
            { 1, 8, 0 }
     };

    bool PatternMatch(Cell cell, int[,] pattern, int offset = -1)
    {
        pattern = FlipBlock(pattern);
        if (offset == -1)
            offset = ((pattern.GetLength(0) - 1) / 2); //centre
        for (int i = 0; i < pattern.GetLength(1); i++)
        {
            for (int j = 0; j < pattern.GetLength(0); j++)
            {
                Cell gridCell = new Cell(cell.X + (i - offset), cell.Y + (j - offset)); //gridcell is centred on 1,1 in pattern
                if (pattern[j, i] == 0)
                    continue;
                if (OffMap(gridCell))
                {
                    if (pattern[j, i] == (int)CellType.None)
                        continue;
                    else    
                        return false;
                }

                if (!grid[gridCell.Y, gridCell.X].HasLayer((CellType)pattern[j, i]))
                {
                    return false;
                }
            }
        }
        return true;
    }


    public WallType WallSpriteForPosition(int x, int y)
    {
        return WallSpriteForPosition(new Cell(x, y));
        ;
    }

    public WallType WallSpriteForPosition(Cell cell)
    {
        if (PatternMatch(cell, top))
            return WallType.Top;
        if (PatternMatch(cell, topright))
            return WallType.TopRight;
        if (PatternMatch(cell, right))
            return WallType.Right;
        if (PatternMatch(cell, bottomright))
            return WallType.BottomRight;
        if (PatternMatch(cell, bottom))
            return WallType.Bottom;
        if (PatternMatch(cell, bottomleft))
            return WallType.BottomLeft;
        if (PatternMatch(cell, left))
            return WallType.Left;
        if (PatternMatch(cell, topleft))
            return WallType.TopLeft;

        if (PatternMatch(cell, innerbottomright))
            return WallType.InnerBottomRight;
        if (PatternMatch(cell, innerbottomleft))
            return WallType.InnerBottomLeft;
        if (PatternMatch(cell, innertopright))
            return WallType.InnerTopRight;
        if (PatternMatch(cell, innertopleft))
            return WallType.InnerTopLeft;

        return WallType.None;
    }


    public bool ValidLocation(PathFinder.Node.NLocation node)
    {
        return !OffMap(node);
    }

    bool OffMap(PathFinder.Node.NLocation end)
    {
        if (end.x < 0 || end.x >= width)
            return true;
        if (end.y < 0 || end.y >= height)
            return true;
        return false;
    }

    bool OffMap(Cell end)
    {
        if (end.X < 0 || end.X >= width)
            return true;
        if (end.Y < 0 || end.Y >= height)
            return true;
        return false;
    }
    public bool IsBlocked(PathFinder.Node.NLocation end, IMovableUnit moveableUnit)
    {
        //if (end.x < 2 || end.x >= width - 2)
        //    return true;
        //if (end.y < 2 || end.y >= height - 2)
        //    return true;

        if (OffMap(end))
            return true;
        return HasLayer(end.x, end.y, CellType.Wall);
    }


    public int GetTerrainPenalty(IMovableUnit moveableUnit, PathFinder.Node.NLocation location)
    {
        if (OffMap(location))
            return 10;
        if (grid[location.y, location.x].LayerMask == (int)CellType.Wall)
            return 5;
        return 0;
    }



    public Vector3 LocationToVector(Cell loc)
    {
        return LocationToVector(new PathFinder.Node.NLocation(loc.X, loc.Y));//  new Vector3(loc.X, loc.Y, 0);
    }

    public Vector3 LocationToVector(PathFinder.Node.NLocation loc)
    {
        return new Vector3(loc.x + 0.5f, loc.y + 0.5f, 0);
    }

    public PathFinder.Node.NLocation VectorToLocation(Vector3 vec)
    {
        return new PathFinder.Node.NLocation(System.Convert.ToInt32(vec.x - 0.5f), System.Convert.ToInt32(vec.y - 0.5f));
    }

    public struct Cell
    {
        public int X;
        public int Y;
        public Cell(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", X, Y);
        }
    }

    struct RoomSize
    {
        public int Width;
        public int Height;
        public RoomSize(int width, int height)
        {
            Width = width;
            Height = height;
        }
    }
    public delegate void ApplyToCell(int x, int y);
}
