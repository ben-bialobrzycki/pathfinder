# PathFinder

An implementation of A* for 2d pathfinding, aimed to be generic enough to integrate with most systems, just need to implement a specific interface.
Also a couple of examples of 2d map generation, through random room placement and perlin noise.

## Features
- options to run over multiple frames to minimise performance impact
- Some effort to minimise GC allocation via object pooling etc.